SELECT
  ROW_NUMBER() OVER() AS unique_id,
  ST_Transform(s.geom, 4326) as geom,
  s.segment_id,
  s.name,
  s.cross_name_start,
  s.cross_name_end,
  s.start_intersection_id,
  s.end_intersection_id,
  s.posted_speed,
  s.total_lanes,
  s.overall_land_use,
  c.surface_condition AS sidewalk_condition_score,
  w.width AS sidewalk_width,
  w.buffer_type AS sidewalk_buffer_type,
  w.buffer_width AS sidewalk_buffer_width
FROM street.segment AS s
  LEFT JOIN pedestrian.sidewalk_segment as w
    ON ST_DWithin(s.geom, w.geom, 100) AND
      pcd_segment_match(s.geom, w.geom, 100)
    LEFT JOIN pedestrian.sidewalk_score as c
    ON ST_DWithin(s.geom, c.geom, 100) AND
      pcd_segment_match(s.geom, c.geom, 100)