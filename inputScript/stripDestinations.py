"""Strip Destinations

This reads the "destinations" layer from ./input/lt_data.gpkg and writes it to a file ./input/destinations.gpkg
"""

import geopandas as gpd

input_path = "./input/lt_data.gpkg"
output_path = "./input/destinations.gpkg"

if __name__ == "__main__":
    destinations_frame: gpd.GeoDataFrame = gpd.read_file(input_path, engine="fiona", layer="destination")
    print(destinations_frame)

    destinations_frame.to_file(output_path, engine="fiona", layer="destination")
