SELECT
  ROW_NUMBER() OVER() AS unique_id,
  ST_Transform(s.geom, 4326) as geom,
  s.segment_id,
  s.name,
  s.cross_name_start,
  s.cross_name_end,
  s.start_intersection_id,
  s.end_intersection_id,
  aadt.aadt AS aadt,
  s.posted_speed,
  s.parking_lane_width,
  s.lanes_per_direction,
  s.functional_classification,
  b.bike_width AS bicycle_facility_width,
  b.path_category AS bicycle_path_category,
  b.buffer_width AS bicycle_buffer_width,
  b.buffer_type AS bicycle_buffer_type,
  i.lane_configuration,
  i.right_turn_length,
  i.bike_approach_alignment as bicycle_approach_alignment
FROM street.segment AS s
  LEFT JOIN bicycle.path_singlepart as b
    ON ST_DWithin(s.geom, b.bike_geom, 100) AND
      pcd_segment_match(s.geom, b.bike_geom, 100)
  LEFT JOIN street.aadt as aadt
    ON aadt.segment_id = s.segment_id
  LEFT JOIN street.intersection_approach as i
    ON s.segment_id = i.segment_id