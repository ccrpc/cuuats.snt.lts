This folder contains a script for creating the input data for the LTS Script.

To run the script first create a file called .env in the inputScript folder.

Then copy the contents of .env.example into .env.

Replace REPLACEME in the DB_PASSWORD variable with the password to the "view" user in our main database.

This password can be found in the password file as "PostGIS PCD View User" in the "GIS" category.

### Installation

If you are running this script for the first time, create a virtual environment with `python -m venv venv`

Then activate this environment with

- Linux: `source venv/bin/activate`
- Windows (cmd): `venv\Scripts\activate.bat`
- Windows (powershell): `venv\Scripts\Activate.ps1`

With the virtual environment active, install the dependencies of the script with `python -m pip install -r requirements.txt`

### Running the Script

To the run the script, activate the virtual environment and run `python createInputs.py`
