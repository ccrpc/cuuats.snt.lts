"""GPKG Compare

This script, given an inputPath and a compare path, will make sure that the GPKG
files in the two directories are equal.

Usage: python3 compareGPKG.py inputPath comparePath
"""

import geopandas as geopd
import geopandas.testing

# import os
import sys
import fiona


def compareLayers(fileOne, fileTwo):
    """Compare Layers

    This function will compare every layer of the two geopackage files, after
    confirming the two have the same layer names
    """
    print("Comparing {} and {}\n".format(fileOne, fileTwo))
    layersOne = fiona.listlayers(fileOne)
    layersTwo = fiona.listlayers(fileTwo)

    layersDifferent = [layer for layer in layersOne + layersTwo if layer not in layersOne or layer not in layersTwo]
    if len(layersDifferent) != 0:
        print("{} and {} have different Layers.".format(fileOne, fileTwo))
        print("{} layers: {}".format(fileOne, layersOne))
        print("{} layers: {}".format(fileTwo, layersTwo))
        return False

    for layer in layersOne:
        print("Comparing on {} layer".format(layer))

        print("Reading Files")
        geoFrameOne = geopd.read_file(
            fileOne,
            layer=layer,
            engine="fiona",
            driver="GPKG",
        )
        geoFrameTwo = geopd.read_file(
            fileTwo,
            layer=layer,
            engine="fiona",
            driver="GPKG",
        )

        print("Running Assertion")

        equal = geopandas.testing.assert_geodataframe_equal(geoFrameOne, geoFrameTwo)
        print("The two data Frames, on layer {}, are equal: {}".format(layer, equal))


if __name__ == "__main__":
    inputPath = sys.argv[1]
    comparePath = sys.argv[2]

    compareLayers(inputPath, comparePath)

    # inputFiles = list(filter(lambda x: x.endswith(".gpkg"), os.listdir(inputPath)))
    # compareFiles = list(filter(lambda x: x.endswith(".gpkg"), os.listdir(comparePath)))

    # for file in inputFiles:
    #     if file not in compareFiles:
    #         print("Error, {} has no file to compare to.".format(file))
    #         print("Please ensure all GPKG files in {} have a representation in {}".format(inputPath, comparePath))
    #         exit()

    #     readAndCompare(inputPath + file, comparePath + file)
