SELECT
  ROW_NUMBER() OVER() AS unique_id,
  ST_Transform(s.geom, 4326) as geom,
  s.segment_id,
  s.name,
  s.cross_name_start,
  s.cross_name_end,
  s.start_intersection_id,
  s.end_intersection_id,
  s.lanes_per_direction,
  s.overall_land_use,
  aadt.aadt as aadt,
  b.bike_width as bicycle_facility_width,
  b.path_category as bicycle_path_category,
  b.buffer_width as bicycle_buffer_width,
  b.buffer_type as bicycle_buffer_type,
  b.path_type as bicycle_path_type,
  c.xing_type_ as railroad_crossing_type,
  hvc.heavy_vehicle_count,
  sign.sign_type as road_sign_type,
  bus_stop_count,
  pc.pavement_condition,
  approach.lane_configuration,
  vc.volume_capacity
FROM street.segment as s
LEFT JOIN bicycle.path_singlepart AS b
  ON ST_DWithin(s.geom, b.bike_geom, 100) AND
    pcd_segment_match(s.geom, b.bike_geom, 100)
LEFT JOIN street.railroad_crossing as c
  ON ST_DWithin(s.geom, c.geom, 10)
LEFT JOIN street.latest_heavy_vehicle_count as hvc
  ON hvc.segment_id = s.segment_id
LEFT JOIN street.latest_pavement_condition as pc
  ON pc.segment_id = s.segment_id
LEFT JOIN street.sign as sign
  ON ST_DWithin(sign.geom, s.geom, 35)
LEFT JOIN street.intersection_approach as approach
  ON s.segment_id = approach.segment_id
LEFT JOIN street.latest_volume_capacity as vc
  ON s.segment_id = vc.segment_id::integer
LEFT JOIN street.latest_aadt as aadt
  ON aadt.segment_id = s.segment_id
LEFT JOIN
  (
    SELECT
      COUNT(stop_id)::INT AS bus_stop_count,
      intersection_id
    FROM (
      SELECT DISTINCT stop.stop_id,
        (SELECT intersection.intersection_id
         FROM street.intersection as intersection
         ORDER BY stop.geom <#> intersection.geom LIMIT 1) AS intersection_id
      FROM transit.mtd_stop AS stop
      ORDER BY intersection_id
    ) temp
    GROUP BY intersection_id
  ) as count
ON count.intersection_id = approach.intersection_id