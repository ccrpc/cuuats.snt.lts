SELECT
  ROW_NUMBER() OVER() AS _uid_,
  intersection_id,
  ST_X(ST_TRANSFORM(geom, 4326)) as x,
  ST_Y(ST_TRANSFORM(geom, 4326)) as y,
  geom
FROM street.intersection