"""Create Input

This script creates the file that the level of traffic stress expects.
The Level of Traffic Stress expects 5 layers:
- plts:
    PLTS stands for "Pedestrian Level of Traffic Stress" and is formed by the plts SQL query.
    That query is found in the plts.sql file in this folder.
    It is also joined with the crossing SQL found in the crossing.sql file on segment_id

- blts
    BLTS stands for "Bicycle Level of Traffic Stress".
    This layer is formed by the blts SQL query joined with the crossing SQL query

- alts
    ALTS stands for "Automobile Level of Traffic Stress" and is formed by the alts SQL Query.
    The alts layer does not use any information from the crossing sql query.

- nodes
    This layer is a layer of intersections with x and y values for the geometries. It is created by nodes.sql.

- destination
    This layer has information on all of the businesses, healthcare facilities, parks, etc in the champaign urbana area.
    This layer is created separately by a different process, not this script.
    This script reads in this information from ./input/destinations.gpkg,
      which is a geopackage file with only one layer titled "destination".

## Renames ##
The input script applies a few renaming rules to the results of the crossing sql before the merge.
The renaming rules are:
    "aadt" -> "crossing_aadt",
    "posted_speed" -> "crossing_speed",
    "functional_classification" -> "crossing_functional_classification",
    "median_refuge_type" -> "intersection_median_refuge",
    "control_type" -> "intersection_control_type"

Note: We have the nodes layer have a geometry here for comparison purposes though it is not strictly necessary.
"""

from sqlalchemy import create_engine
from dotenv import load_dotenv
import geopandas as gpd
import pandas.io.sql as pdsql
import pandas as pd
import os

# LTS Expected Data Structure
# This is the expected structure of the data as given in the documentation
# we will check that the input data matches this structure

expected_blts_structure = [
    "segment_id",
    "name",
    "cross_name_start",
    "cross_name_end",
    "geom",
    "aadt",
    "bicycle_facility_width",
    "bicycle_path_category",
    "bicycle_buffer_width",
    "bicycle_buffer_type",
    # "bicycle_path_type",
    "bicycle_approach_alignment",
    # "bus_trips_total",
    # "crossing_aadt",
    # "crossing_functional_classification",
    "crossing_speed",
    "functional_classification",
    # "heavy_vehicle_count",
    "intersection_control_type",
    "intersection_median_refuge",
    "lane_configuration",
    "lanes_per_direction",
    "max_lanes_crossed",
    # "overall_land_use",
    "parking_lane_width",
    # "pavement_condition",
    "posted_speed",
    # "railroad_crossing_type",
    "right_turn_length",
    # "road_sign_type",
    # "sidewalk_buffer_width",
    # "sidewalk_condition_score",
    # "sidewalk_width",
    # "sidewalk_buffer_type",
    # "total_lanes",
    # "volume_capacity",
]

expected_alts_structure = [
    "segment_id",
    "name",
    "cross_name_start",
    "cross_name_end",
    "geom",
    "aadt",
    # "bicycle_facility_width",
    "bicycle_path_category",
    "bicycle_buffer_width",
    # "bicycle_buffer_type",
    "bicycle_path_type",
    # "bicycle_approach_alignment",
    "bus_trips_total",
    # "crossing_aadt",
    # "crossing_functional_classification",
    # "crossing_speed",
    # "functional_classification",
    "heavy_vehicle_count",
    # "intersection_control_type",
    # "intersection_median_refuge",
    "lane_configuration",
    "lanes_per_direction",
    # "max_lanes_crossed",
    # "overall_land_use",
    # "parking_lane_width",
    "pavement_condition",
    # "posted_speed",
    "railroad_crossing_type",
    # "right_turn_length",
    "road_sign_type",
    # "sidewalk_buffer_width",
    # "sidewalk_condition_score",
    # "sidewalk_width",
    # "sidewalk_buffer_type",
    # "total_lanes",
    "volume_capacity",
]

expected_plts_structure = [
    "segment_id",
    "name",
    "cross_name_start",
    "cross_name_end",
    "geom",
    # "aadt",
    # "bicycle_facility_width",
    # "bicycle_path_category",
    # "bicycle_buffer_width",
    # "bicycle_buffer_type",
    # "bicycle_path_type",
    # "bicycle_approach_alignment",
    # "bus_trips_total",
    "crossing_aadt",
    "crossing_functional_classification",
    "crossing_speed",
    # "functional_classification",
    # "heavy_vehicle_count",
    "intersection_control_type",
    "intersection_median_refuge",
    # "lane_configuration",
    # "lanes_per_direction",
    "max_lanes_crossed",
    "overall_land_use",
    # "parking_lane_width",
    # "pavement_condition",
    "posted_speed",
    # "railroad_crossing_type",
    # "right_turn_length",
    # "road_sign_type",
    "sidewalk_buffer_width",
    "sidewalk_condition_score",
    "sidewalk_width",
    "sidewalk_buffer_type",
    "total_lanes",
    # "volume_capacity",
]

if __name__ == "__main__":
    # Get database variables from .env vile
    print("Loading Database information from .env file")
    load_dotenv()

    DB_HOST = os.environ.get("DB_HOST")
    DB_NAME = os.environ.get("DB_NAME")
    DB_USER = os.environ.get("DB_USER")
    DB_PASSWORD = os.environ.get("DB_PASSWORD")
    DB_PORT = os.environ.get("DB_PORT")

    # Create engine to connect to database
    engine = create_engine(f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}")

    # Read in the 'destination' layer from the input file
    print("Reading in destination layer from ./input/destination.gpkg")
    destination_df: gpd.GeoDataFrame = gpd.read_file("./input/destinations.gpkg", engine="fiona", layer="destination")

    # Read in the SQL Queries from the text files
    with open("nodes.sql", "r") as sql_file:
        NODES_QUERY = sql_file.read()

    with open("alts.sql", "r") as sql_file:
        ALTS_QUERY = sql_file.read()

    with open("blts.sql", "r") as sql_file:
        BLTS_QUERY = sql_file.read()

    with open("plts.sql", "r") as sql_file:
        PLTS_QUERY = sql_file.read()

    with open("crossing.sql", "r") as sql_file:
        CROSSING_QUERY = sql_file.read()

    # Create GeoDataFrames from the sql queries
    print("Pulling Data frames from POSTGIS")

    print("Running nodes query...")
    nodes_df: gpd.GeoDataFrame = gpd.GeoDataFrame.from_postgis(NODES_QUERY, con=engine, crs=4326)

    print("Running alts query...")
    alts_df: gpd.GeoDataFrame = gpd.GeoDataFrame.from_postgis(ALTS_QUERY, con=engine, crs=4326)

    print("Running blts query...")
    blts_df: gpd.GeoDataFrame = gpd.GeoDataFrame.from_postgis(BLTS_QUERY, con=engine, crs=4326)

    print("Running plts query...")
    plts_df: gpd.GeoDataFrame = gpd.GeoDataFrame.from_postgis(PLTS_QUERY, con=engine, crs=4326)

    # The crossing query does not return a geom so we are pulling it into a pandas dataframe instead.
    print("Running crossing query...")
    crossings_df: pd.DataFrame = pdsql.read_sql(CROSSING_QUERY, con=engine)
    crossings_df.rename(
        columns={
            "aadt": "crossing_aadt",
            "posted_speed": "crossing_speed",
            "functional_classification": "crossing_functional_classification",
            "median_refuge_type": "intersection_median_refuge",
            "control_type": "intersection_control_type",
        },
        inplace=True,
    )

    # Doing a variable duplication to accommodate some renames in the data that hasn't been updated in the script yet
    # TODO Remove this once the script has been updated to use bus_stop_count
    alts_df["bus_trips_total"] = alts_df["bus_stop_count"]

    # Merge the crossing data frame with the PLTS and BLTS data frames.
    print("Merging Crossing Data with plts and blts")

    blts_df = blts_df.merge(crossings_df, how="left", on="segment_id")
    plts_df = plts_df.merge(crossings_df, how="left", on="segment_id")

    # Check the columns of our dataframes against the columns the LTS script expects
    # We just want the expected columns to be present in the data.
    print("Comparing Dataframe Columns with Expected Columns")

    blts_structure = blts_df.columns.values.tolist()
    plts_structure = plts_df.columns.values.tolist()
    alts_structure = alts_df.columns.values.tolist()

    assert all(x in blts_structure for x in expected_blts_structure), [
        "blts data not in expected data structure format",
        blts_structure,
    ]
    assert all(x in plts_structure for x in expected_plts_structure), [
        "plts data not ins expected data structure format",
        plts_structure,
    ]
    assert all(x in alts_structure for x in expected_alts_structure), [
        "alts data not ins expected data structure format",
        alts_structure,
    ]

    # Write our data frames to the output file
    print("Writing Output Geopackage")

    outpath = "./output/lts_input.gpkg"

    if os.path.exists(outpath):
        print("Output file exists already, removing and recreating")
        os.remove(outpath)

    nodes_df.to_file(outpath, drive="fiona", layer="nodes")
    destination_df.to_file(outpath, drive="fiona", layer="destination")
    alts_df.to_file(outpath, drive="fiona", layer="alts")
    blts_df.to_file(outpath, drive="fiona", layer="blts")
    plts_df.to_file(outpath, drive="fiona", layer="plts")

    print("===\nDone, input file created!")
    print("You can find it at {}".format(outpath))
