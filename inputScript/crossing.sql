WITH approach_angle AS (
        SELECT approach.segment_id,
           approach.intersection_id,
           segment.posted_speed,
           CASE
             WHEN segment.functional_classification::text = 'Interstate'::text THEN 1
             WHEN segment.functional_classification::text = 'Major Arterial'::text THEN 2
             WHEN segment.functional_classification::text = 'Minor Arterial'::text THEN 3
             WHEN segment.functional_classification::text = 'Major Collector'::text THEN 4
             WHEN segment.functional_classification::text = 'Minor Collector'::text THEN 5
             WHEN segment.functional_classification::text = 'Local Road or Street'::text THEN 6
           END AS functional_classification,
           aadt.aadt AS aadt,
           intersection.control_type,
           approach.median_refuge_type,
               CASE
                   WHEN approach.lane_configuration IS NOT NULL THEN char_length(approach.lane_configuration::text)
                   ELSE segment.total_lanes
               END AS lanes,
           degrees(st_azimuth(st_pointn(segment.geom,
               CASE
                   WHEN approach.intersection_id = segment.start_intersection_id THEN 1
                   ELSE '-1'::integer
               END), st_pointn(segment.geom,
               CASE
                   WHEN approach.intersection_id = segment.start_intersection_id THEN 2
                   ELSE '-2'::integer
               END))) AS angle
          FROM street.intersection_approach approach
            LEFT JOIN street.segment segment ON approach.segment_id = segment.segment_id
            JOIN street.intersection intersection ON approach.intersection_id = intersection.intersection_id
            LEFT JOIN street.aadt aadt ON aadt.segment_id = segment.segment_id
       )
SELECT seg.segment_id,
   crossed.control_type,
   crossed.median_refuge_type,
   max(crossed.aadt) AS aadt,
   CASE
     WHEN min(crossed.functional_classification) = 1 THEN 'Interstate'
     WHEN min(crossed.functional_classification) = 2 THEN 'Major Arterial'
     WHEN min(crossed.functional_classification) = 3 THEN 'Minor Arterial'
     WHEN min(crossed.functional_classification) = 4 THEN 'Major Collector'
     WHEN min(crossed.functional_classification) = 5 THEN 'Minor Collector'
     WHEN min(crossed.functional_classification) = 6 THEN 'Local Road or Street'
   END
   AS functional_classification,
   max(crossed.posted_speed) AS posted_speed,
   max(COALESCE(crossed.lanes, 0)) AS max_lanes_crossed
  FROM approach_angle seg
    LEFT JOIN approach_angle crossed ON seg.intersection_id = crossed.intersection_id AND seg.segment_id <> crossed.segment_id AND
       CASE
           WHEN abs(seg.angle - crossed.angle) > 180::double precision THEN 360::double precision - abs(seg.angle - crossed.angle)
           ELSE abs(seg.angle - crossed.angle)
       END < 135::double precision
 GROUP BY seg.segment_id, crossed.control_type, crossed.median_refuge_type
 ORDER BY seg.segment_id;