import pandas as pd
import json
import os


class Config:
    def __init__(self, c):
        # BLTS
        # Off-Street Trail
        self.vertical_trail = c.get('vertical_trail')
        self.off_street_trail = c.get('off_street_trail')
        self.off_street_trail_score = int(c.get('off_street_trail_score'))
        # Functional Classification
        self.functional_class_lookup = c.get('functional_class_lookup')
        # Bike Lane with Adjacent Parking Lane Criteria Scorintg Table
        self.bl_adj_pk_table_one_lane = pd.DataFrame(
            c.get('bl_adj_pk_table_one_lane'))
        self.bl_adj_pk_aadt_scale = self._parse_scale(
            c.get('bl_adj_pk_aadt_scale'))
        self.bl_adj_pk_width_scale = self._parse_scale(
            c.get('bl_adj_pk_width_scale'))
        self.bl_adj_pk_table_two_lanes = pd.DataFrame(
            c.get('bl_adj_pk_table_two_lanes'))
        self.bl_adj_pk_two_width_scale = self._parse_scale(
            c.get('bl_adj_pk_two_width_scale'))
        # Bike Lane without Adjacent Parking Lane Criteria Scoring Table
        self.bl_no_adj_pk_table_one_lane = pd.DataFrame(
            c.get('bl_no_adj_pk_table_one_lane'))
        self.bl_no_adj_pk_aadt_scale = self._parse_scale(
            c.get('bl_no_adj_pk_aadt_scale'))
        self.bl_no_adj_pk_width_scale = self._parse_scale(
            c.get('bl_no_adj_pk_width_scale'))
        self.bl_no_adj_pk_table_two_lanes = pd.DataFrame(
            c.get('bl_no_adj_pk_table_two_lanes'))
        self.bl_no_adj_pk_two_width_scale = self._parse_scale(
            c.get('bl_no_adj_pk_two_width_scale'))
        # Urban/Suburban Mixed Traffic Criteria Scoring Table
        self.mixed_traffic_table = pd.DataFrame(c.get('mixed_traffic_table'))
        self.urban_fix_traffic_aadt_scale = self._parse_scale(
            c.get('urban_fix_traffic_aadt_scale'))
        self.urban_fix_traffic_lane_scale = self._parse_scale(
            c.get('urban_fix_traffic_lane_scale'))
        # turn threshold before lane turn criteria is considered
        self.turn_threshold = c.get('turn_threshold')
        # Right Turn Lane Criteria Scoring Table
        self.right_turn_lane_table = c.get('right_turn_lane_table')
        # Left Turn Lane Criteria Scoring Table
        self.left_turn_lane_dual_shared_table = pd.Series(
            c.get('left_turn_lane_dual_shared_table'))
        self.left_turn_lane_dual_shared_speed_scale = self._parse_scale(
            c.get('left_turn_lane_dual_shared_speed_scale'))
        self.left_turn_lane_table = pd.DataFrame(c.get('left_turn_lane_table'))
        self.left_turn_lane_speed_scale = self._parse_scale(
            c.get('left_turn_lane_speed_scale'))
        self.left_turn_lane_lane_crossed_scale = self._parse_scale(
            c.get('left_turn_lane_lane_crossed_scale'))
        # Unsignalized Intersection Crossing Without a Median Refuge Criteria
        self.crossing_no_median_table = pd.DataFrame(
            c.get('crossing_no_median_table'))
        self.crossing_no_median_speed_scale = self._parse_scale(
            c.get('crossing_no_median_speed_scale'))
        self.crossing_no_median_lane_scale = self._parse_scale(
            c.get('crossing_no_median_lane_scale'))
        # Unsignalized Intersection Crossing With a Median Refuge Criteria
        self.crossing_has_median_table = pd.DataFrame(
            c.get('crossing_has_median_table'))
        self.crossing_has_median_speed_scale = self._parse_scale(
            c.get('crossing_has_median_speed_scale'))
        self.crossing_has_median_lane_scale = self._parse_scale(
            c.get('crossing_has_median_lane_scale'))
        # PLTS
        # Sidewalk Condition
        self.sidewalk_condition_table = pd.DataFrame(
            c.get('sidewalk_condition_table'))
        self.sidewalk_condition_width_scale = self._parse_scale(
            c.get('sidewalk_condition_width_scale'))
        self.sidewalk_condition_condition_scale = self._parse_scale(
            c.get('sidewalk_condition_condition_scale'))
        # Physical Buffer Type
        self.buffer_type_table = pd.DataFrame(c.get('buffer_type_table'))
        self.buffer_type_lookup = c.get('buffer_type_lookup')
        self.buffer_type_type_scale = pd.Index(c.get('buffer_type_type_scale'))
        self.buffer_type_speed_scale = self._parse_scale(
            c.get('buffer_type_speed_scale'))
        # Total Buffer Width
        self.buffer_width_table = pd.DataFrame(c.get('buffer_width_table'))
        self.buffer_width_lane_scale = self._parse_scale(
            c.get('buffer_width_lane_scale'))
        self.buffer_width_width_scale = self._parse_scale(
            c.get('buffer_width_width_scale'))
        # Collector and local unsignalized intersection crossing
        self.collector_crossing_table = pd.DataFrame(
            c.get('collector_crossing_table'))
        self.collector_crossing_speed_scale = self._parse_scale(
            c.get('collector_crossing_speed_scale'))
        self.collector_crossing_lane_scale = self._parse_scale(
            c.get('collector_crossing_lane_scale'))
        # Arterial unsignalized intersection crossing - 2 lanes
        self.arterial_crossing_two_lanes_table = pd.DataFrame(
            c.get('arterial_crossing_two_lanes_table'))
        self.arterial_crossing_speed_scale = self._parse_scale(
            c.get('arterial_crossing_speed_scale'))
        self.arterial_crossing_two_lanes_aadt_scale = self._parse_scale(
            c.get('arterial_crossing_two_lanes_aadt_scale'))
        # Arterial unsignalized intersection crossing - 3 lanes
        self.arterial_crossing_three_lanes_table = pd.DataFrame(
            c.get('arterial_crossing_three_lanes_table'))
        self.arterial_crossing_three_lanes_aadt_scale = self._parse_scale(
            c.get('arterial_crossing_three_lanes_aadt_scale'))
        # Land Use
        self.landuse_lookup = c.get('landuse_lookup')
        # ALTS
        # On street facility
        self.on_street_facility = c.get('on_street_facility')
        self.railroad_crossing_score = c.get('railroad_crossing_score')
        self.crossing_type = c.get('crossing_type')
        # Heavy vehicle traffic
        self.heavy_vehicle_table = pd.Series(c.get('heavy_vehicle_table'))
        self.heavy_vehicle_scale = self._parse_scale(
            c.get('heavy_vehicle_scale'))
        # Bus trips
        self.bus_trips_table = pd.Series(c.get('bus_trips_table'))
        self.bus_trips_scale = self._parse_scale(c.get('bus_trips_scale'))
        # School Sign Type
        self.school_sign_type = c.get('school_sign_type')
        self.school_zone_score = c.get('school_zone_score')
        # Pavement Condition
        self.pavement_condition_table = pd.Series(
            c.get('pavement_condition_table'))
        self.pavement_condition_scale = pd.Index(
            c.get('pavement_condition_scale'))
        self.pavement_condition_none_score = \
            c.get('pavement_condition_none_score')
        self.vehicle_turn_lane_score = c.get('vehicle_turn_lane_score')
        # Street Width
        self.street_width_table = pd.Series(c.get('street_width_table'))
        self.street_width_scale = self._parse_scale(
            c.get('street_width_scale'))
        self.street_width_none_score = c.get('street_width_none_score')
        # Volumn capacity
        self.volume_capacity_table = pd.Series(c.get('volume_capacity_table'))
        self.volume_capacity_scale = self._parse_scale(
            c.get('volume_capacity_scale'))
        self.volume_capacity_none_score = c.get('volume_capacity_none_score')

    def _parse_scale(self, ary):
        start = ary.copy()
        start.insert(0, -float('Inf'))
        end = ary.copy()
        end.append(float('Inf'))
        return pd.IntervalIndex.from_arrays(
            start, end
        )


DIR = os.path.dirname(os.path.abspath(__file__))


with open(os.path.join(DIR, 'config.json')) as json_file:
    data = json.load(json_file)
conf = Config(data)
