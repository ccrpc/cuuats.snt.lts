import sys
import fiona
import pandas as pd
import geopandas as gpd
from .model import Segment, Approach, BikePath, Crossing, Sidewalk, Intersection, RailroadCrossing, Sign
from progress.bar import Bar
import subprocess


def functional_classification_to_number(df, column):
    df[column] = df[column].replace('Interstate', 1)
    df[column] = df[column].replace('Major Arterial', 2)
    df[column] = df[column].replace('Minor Arterial', 3)
    df[column] = df[column].replace('Major Collector', 4)
    df[column] = df[column].replace('Minor Collector', 5)
    df[column] = df[column].replace('Local Road or Street', 6)
    return df


def functional_classification_to_text(df, column):
    df[column] = df[column].replace(1, 'Interstate')
    df[column] = df[column].replace(2, 'Major Arterial')
    df[column] = df[column].replace(3, 'Minor Arterial')
    df[column] = df[column].replace(4, 'Major Collector')
    df[column] = df[column].replace(5, 'Minor Collector')
    df[column] = df[column].replace(6, 'Local Road or Street')
    return df


def combine_records(path, out_path):
    blts = gpd.read_file(path, layer='blts_score', driver='GPKG')
    plts = gpd.read_file(path, layer='plts_score', driver='GPKG')
    alts = gpd.read_file(path, layer='alts_score', driver='GPKG')

    blts = functional_classification_to_number(
                        blts, 'functional_classification')
    blts = functional_classification_to_number(
                        blts, 'crossing_functional_classification')
    plts = functional_classification_to_number(
                        plts, 'crossing_functional_classification')
    final_df = pd.DataFrame(blts[['segment_id', 'geometry']])
    final_df.set_index('segment_id', inplace=True)

    max_columns = (
        'aadt', 'blts_score', 'plts_score', 'alts_score', 'bus_trips_total',
        'crossing_speed', 'heavy_vehicle_count', 'max_lanes_crossed',
        'right_turn_length', 'crossing_aadt'
    )

    min_columns = (
        'bicycle_buffer_width', 'bicycle_facility_width',
        'functional_classification', 'parking_lane_width',
        'sidewalk_buffer_width', 'sidewalk_condition_score', 'sidewalk_width',
        'volume_capacity', 'crossing_functional_classification'
    )

    first_columns = (
        'name', 'cross_name_start', 'cross_name_end', 'bicycle_buffer_type',
        'bicycle_path_category', 'bicycle_path_type',
        'bicycle_approach_alignment', 'intersection_control_type',
        'intersection_median_refuge', 'lane_configuration',
        'lanes_per_direction', 'overall_land_use', 'pavement_condition',
        'posted_speed', 'railroad_crossing_type', 'road_sign_type',
        'sidewalk_buffer_type', 'total_lanes', 'start_intersection_id',
        'end_intersection_id'
    )
    # TODO: Does this actually make sense? Switching from "final_df" to "df"?
    # Final_df will have the combined values (max, min, or first) from df
    # I have moved the creation of df down to right before it is used for clarity

    df = pd.concat([blts, plts, alts])
    for column in max_columns:
        final_df[column] = df[['segment_id', column]].groupby(
                                                ['segment_id'])[column].max()

    for column in min_columns:
        final_df[column] = df[['segment_id', column]].groupby(
                                                ['segment_id'])[column].min()

    for column in first_columns:
        final_df[column] = df[['segment_id', column]].groupby(
                                                ['segment_id'])[column].first()

    final_df = functional_classification_to_text(
                        final_df, 'functional_classification')
    final_df = functional_classification_to_text(
                        final_df, 'crossing_functional_classification')
    final_df = gpd.GeoDataFrame(final_df, geometry='geometry')
    final_df = final_df.reset_index()
    with fiona.open(path, 'r', layer='blts_score') as src:
        final_df.crs = src.crs

    print('Writing final table to disk...')
    final_df.to_file(out_path,
                     driver='GPKG',
                     layer='combined')


def find_max_score(records, compare_field):
    worst_records = {}
    for record in records:
        worst_record = worst_records.get('segment_id') or None
        if worst_record is None:
            worst_records[record['properties']['segment_id']] = record
        elif worst_record['properties'][compare_field] < \
                record['properties'][compare_field]:
            worst_records[record['properties']['segment_id']] = record
    return list(worst_records.values())


def get_blts(path):
    with fiona.open(path, 'r', layer='blts') as src:
        driver = src.driver
        crs = src.crs
        schema = src.schema
        schema['properties']['blts_score'] = 'int'
        records = []

        bar = Bar('Calculating BLTS score', max=len(src))
        for f in src:
            data = f['properties']
            segment = Segment(
                    lanes_per_direction=data['lanes_per_direction'],
                    parking_lane_width=data['parking_lane_width'],
                    aadt=data['aadt'],
                    functional_classification=data[
                        'functional_classification'],
                    posted_speed=data['posted_speed'])
            approaches = [Approach(
                    lane_configuration=data['lane_configuration'],
                    right_turn_length=data['right_turn_length'],
                    bicycle_approach_alignment=data[
                        'bicycle_approach_alignment'])]
            crossings = [Crossing(
                    crossing_speed=data['crossing_speed'],
                    lanes_crossed=data['max_lanes_crossed'],
                    control_type=data['intersection_control_type'],
                    median=data['intersection_median_refuge'])]
            bike_paths = [BikePath(
                    width=data['bicycle_facility_width'],
                    path_category=data['bicycle_path_category'],
                    buffer_width=data['bicycle_buffer_width'],
                    buffer_type=data['bicycle_buffer_type'])]

            score = segment.blts_score(approaches,
                                       crossings,
                                       bike_paths,
                                       10000)
            f['properties']['blts_score'] = int(score)
            records.append(f)
            bar.next()
        bar.finish()

    print('Writing BLTS score to disk...')
    with fiona.open(path,
                    'w',
                    driver=driver,
                    layer='blts_score',
                    crs=crs,
                    schema=schema) as dest:
        dest.writerecords(find_max_score(records, 'blts_score'))


def get_plts(path):
    with fiona.open(path, 'r', layer='plts') as src:
        driver = src.driver
        crs = src.crs
        schema = src.schema
        schema['properties']['plts_score'] = 'int'
        records = []
        bar = Bar('Calculating PLTS score', max=len(src))
        for f in src:
            data = f['properties']
            segment = Segment(
                    total_lanes=data['total_lanes'],
                    posted_speed=data['posted_speed'],
                    overall_land_use=data['overall_land_use'])
            sidewalks = [Sidewalk(
                    sidewalk_width=data['sidewalk_width'],
                    buffer_type=data['sidewalk_buffer_type'],
                    buffer_width=data['sidewalk_buffer_width'],
                    sidewalk_condition_score=data['sidewalk_condition_score'])]
            crossings = [Crossing(
                crossing_speed=data['crossing_speed'],
                control_type=data['intersection_control_type'],
                lanes=data['max_lanes_crossed'],
                median=data['intersection_median_refuge'],
                functional_classification=data[
                    'crossing_functional_classification'],
                aadt=data['crossing_aadt'])]

            score = segment.plts_score(crossings, sidewalks)
            f['properties']['plts_score'] = int(score)
            records.append(f)
            bar.next()
    bar.finish()
    print('Writing PLTS score to disk...')
    with fiona.open(path,
                    'w',
                    driver=driver,
                    layer='plts_score',
                    crs=crs,
                    schema=schema) as dest:
        dest.writerecords(find_max_score(records, 'plts_score'))


def get_alts(path):
    with fiona.open(path, 'r', layer='alts') as src:
        driver = src.driver
        crs = src.crs
        schema = src.schema
        schema['properties']['alts_score'] = 'float'
        records = []
        bar = Bar('Calculating ALTS score', max=len(src))
        for f in src:
            data = f['properties']

            segment = Segment(
                        aadt=data['aadt'],
                        heavy_vehicle_count=data['heavy_vehicle_count'],
                        pavement_condition=data['pavement_condition'],
                        lanes_per_direction=data['lanes_per_direction'],
                        volume_capacity=data['volume_capacity'],
                        overall_landuse=data['overall_land_use'],
                        bus_trips_total=data['bus_trips_total'])
            intersections = [Intersection()]
            bike_paths = [BikePath(
                        width=data['bicycle_facility_width'],
                        path_category=data['bicycle_path_category'],
                        buffer_width=data['bicycle_buffer_width'],
                        buffer_type=data['bicycle_buffer_type'],
                        path_type=data['bicycle_path_type'])]
            railroad_crossing = RailroadCrossing(
                        xing_type=data['railroad_crossing_type'])
            sign = Sign(sign_type=data['road_sign_type'])
            approaches = [Approach(
                        lane_configuration=data['lane_configuration'])]
            score = segment.alts_score(intersections,
                                       bike_paths,
                                       railroad_crossing,
                                       sign,
                                       approaches)

            f['properties']['alts_score'] = score
            records.append(f)
            bar.next()
    bar.finish()
    print('Writing ALTS score to disk...')

    with fiona.open(path,
                    'w',
                    driver=driver,
                    layer='alts_score',
                    crs=crs,
                    schema=schema) as dest:

        dest.writerecords(find_max_score(records, 'alts_score'))


# subprocess just runs the 'ogr2ogr' commands
def main(path, out_path):
    cmd = ['ogr2ogr', '-f', 'GPKG', out_path, path, 'nodes']
    print(cmd)
    subprocess.check_output(cmd)
    cmd = ['ogr2ogr', '-f', 'GPKG', out_path, path, '-append', 'destination']
    print(cmd)
    subprocess.check_output(cmd)
    get_blts(path)
    get_plts(path)
    get_alts(path)
    print('Score calculation completed, data written in {}'.format(
        path
    ))
    combine_records(path, out_path)


# name is a special variable; it will equal 'main' if this is the script being run directly
if __name__ == '__main__':
    if len(sys.argv) == 3:
        main(sys.argv[1], sys.argv[2])
    else:
        print('Need path for geopackage')
