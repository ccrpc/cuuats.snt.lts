# crossing object for lts
from ..config import conf


class Crossing:
    def __init__(self, **kwargs):
        self.crossing_speed = kwargs.get('crossing_speed') or 0
        self.control_type = kwargs.get('control_type') or None
        self.lanes = kwargs.get('lanes') or 0
        self.median = kwargs.get('median') or None
        self.functional_classification = self._get_functional_classification(
            kwargs.get('functional_classification')
        )
        self.aadt = kwargs.get('aadt') or 0

    def _get_functional_classification(self, functional_classification):
        return conf.functional_class_lookup.get(functional_classification) or 6
