import csv
import os


class Zoning:
    # "_get_csv" takes the type of score being run. It pulls up that csv from the directory.
    def _get_csv(self, type):
        with open(
            os.path.dirname(__file__) + "/../zoning_{}.csv".format(type),
            mode="r",
        ) as infile:
            reader = csv.reader(infile)
            self._zone_dict = {
                rows[0]: int(rows[1])
                for rows in reader
                if rows[0] != "zone_code"
            }
#TODO: Should the "zone_code above be "overall_land_use"?
    def get_plts(self, zone):
        self._get_csv("plts")
        return self._zone_dict.get(zone) or 0

    def get_alts(self, zone):
        self._get_csv("alts")
        return self._zone_dict.get(zone) or 0
