from cuuats.snt.lts.script import main
import sys

if __name__ == "__main__":
    if len(sys.argv) == 3:
        main(sys.argv[1], sys.argv[2])
    else:
        print("Need path for geopackage")
